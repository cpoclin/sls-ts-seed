import { ConversationsRepository } from '../common/repositories/conversations.repository'
import { ConversationDocument } from '../common/interfaces/conversation.interface'
import Logger from '../common/utils/Logger'
import { CreateConversationDto } from './dto/create-conversation.dto'

export class ConversationsService {
  constructor(
    private readonly conversationsRepository: ConversationsRepository = new ConversationsRepository()
  ) {}

  async create(body: CreateConversationDto): Promise<ConversationDocument> {
    try {
      return await this.conversationsRepository.create(body)
    } catch (error) {
      Logger.getLogger().error(error)
      throw error
    }
  }

  async close(id: string): Promise<ConversationDocument> {
    try {
      return await this.conversationsRepository.close(id)
    } catch (error) {
      Logger.getLogger().error(error)
      throw error
    }
  }
}
