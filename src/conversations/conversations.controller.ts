import { APIGatewayProxyEvent } from 'aws-lambda'
import { ConversationsService } from './conversations.service'
import { ConversationDocument } from '../common/interfaces/conversation.interface'
import Logger from '../common/utils/Logger'
import { CreateConversationDto } from './dto/create-conversation.dto'

export class ConversationsController {
  constructor(
    private readonly conversationsService: ConversationsService = new ConversationsService()
  ) {}

  async create(event: APIGatewayProxyEvent): Promise<ConversationDocument> {
    try {
      const body: CreateConversationDto = JSON.parse(event.body)
      return await this.conversationsService.create(body)
    } catch (error) {
      Logger.getLogger().error(error)
      throw error
    }
  }

  async close(event: APIGatewayProxyEvent): Promise<ConversationDocument> {
    try {
      const id = event.pathParameters.id
      return await this.conversationsService.close(id)
    } catch (error) {
      Logger.getLogger().error(error)
      throw error
    }
  }
}
