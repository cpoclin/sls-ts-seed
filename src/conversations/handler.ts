import { APIGatewayProxyEvent, Context, Handler } from 'aws-lambda'
import { ConversationsController } from './conversations.controller'
import { getConnection } from '../common/utils/DBConnection'
import Logger from '../common/utils/Logger'
import ResponseUtils from '../common/utils/ResponseUtils'

let cachedMongoDB = null

export const create: Handler = async (
  event: APIGatewayProxyEvent,
  context: Context
): Promise<any> => {
  context.callbackWaitsForEmptyEventLoop = false
  try {
    cachedMongoDB = await getConnection(cachedMongoDB)

    const result = await new ConversationsController().create(event)

    return ResponseUtils.Success(result)
  } catch (error) {
    Logger.getLogger().error(error)
    return ResponseUtils.Error(error)
  }
}

export const close: Handler = async (
  event: APIGatewayProxyEvent,
  context: Context
): Promise<any> => {
  context.callbackWaitsForEmptyEventLoop = false
  try {
    cachedMongoDB = await getConnection(cachedMongoDB)

    const result = await new ConversationsController().close(event)

    return ResponseUtils.Success(result)
  } catch (error) {
    Logger.getLogger().error(error)
    return ResponseUtils.Error(error)
  }
}
