import {
  APIGatewayAuthorizerCallback,
  APIGatewayAuthorizerEvent,
  APIGatewayAuthorizerResult,
  Context,
  Handler,
} from 'aws-lambda'
import { AuthController } from './auth.controller'
import Logger from '../common/utils/Logger'

export const authenticate: Handler = async (
  event: APIGatewayAuthorizerEvent,
  _context: Context,
  callback: APIGatewayAuthorizerCallback
): Promise<any> => {
  try {
    const result = (await new AuthController().authenticate(
      event
    )) as unknown as APIGatewayAuthorizerResult

    callback(null, result)
  } catch (error) {
    Logger.getLogger().error(error)
    callback('Unauthorized')
  }
}
