import jwt, { VerifyOptions, JwtHeader } from 'jsonwebtoken'
import jwks from 'jwks-rsa'
import { APIGatewayAuthorizerEvent, PolicyDocument } from 'aws-lambda'

interface IPayload {
  iss: string
  sub: string
  aud: string
  iat: number
  exp: number
  azp: string
  gty: string
  scope?: unknown
}
interface IDecode {
  header: JwtHeader
  payload: IPayload
  signature: string
}

export class AuthService {
  private jwksUri: string
  private audience: string
  private issuer: string

  constructor() {
    this.jwksUri = process.env.AUTH0_JWKS_URI
    this.audience = process.env.AUTH0_AUDIENCE
    this.issuer = process.env.AUTH0_ISSUER
  }

  private getPolicyDocument = (
    effect: string,
    resource: string
  ): PolicyDocument => {
    return {
      Version: '2012-10-17',
      Statement: [
        {
          Action: 'execute-api:Invoke',
          Effect: effect,
          Resource: '*' || resource,
        },
      ],
    }
  }

  private getToken = (event: APIGatewayAuthorizerEvent): string => {
    if (!event.type || event.type !== 'TOKEN') {
      throw new Error('Expected "event.type" parameter to have value "TOKEN"')
    }

    const tokenString = event.authorizationToken
    if (!tokenString) {
      throw new Error('Expected "event.authorizationToken" parameter to be set')
    }

    const match = tokenString.match(/^Bearer (.*)$/)
    if (!match || match.length < 2) {
      throw new Error(
        `Invalid Authorization token - ${tokenString} does not match "Bearer .*"`
      )
    }
    return match[1]
  }

  authenticate = async (
    event: APIGatewayAuthorizerEvent
  ): Promise<Record<string, unknown>> => {
    const token = this.getToken(event)
    const decoded = jwt.decode(token, { complete: true }) as IDecode

    if (!decoded || !decoded.header || !decoded.header.kid) {
      throw new Error('invalid token')
    }
    const options = {
      audience: this.audience,
      issuer: this.issuer,
      algorithms: ['RS256'],
    } as VerifyOptions
    return this.getSigningKey(decoded.header.kid)
      .then((key) => jwt.verify(token, key, options))
      .then((decoded: IPayload) => {
        return {
          principalId: decoded?.sub,
          policyDocument: this.getPolicyDocument('Allow', event.methodArn),
          context: { scope: decoded.scope },
        }
      })
  }

  private getSigningKey = (kid: string): Promise<string> => {
    const client = jwks({
      // cache: true,
      // rateLimit: true,
      // jwksRequestsPerMinute: 5,
      strictSsl: true,
      jwksUri: this.jwksUri,
    })
    return new Promise((resolve, reject) => {
      client.getSigningKey(kid, (error, key) => {
        if (error) {
          reject(error)
        }
        if ('publicKey' in key) {
          resolve(key.publicKey)
        } else if ('rsaPublicKey' in key) {
          resolve(key.rsaPublicKey)
        }
      })
    })
  }
}
