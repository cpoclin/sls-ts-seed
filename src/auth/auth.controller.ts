import { APIGatewayAuthorizerEvent } from 'aws-lambda'
import { AuthService } from './auth.service'
import Logger from '../common/utils/Logger'

export class AuthController {
  constructor(private readonly authService: AuthService = new AuthService()) {}

  async authenticate(
    event: APIGatewayAuthorizerEvent
  ): Promise<Record<string, unknown>> {
    try {
      return await this.authService.authenticate(event)
    } catch (error) {
      Logger.getLogger().error(error)
      throw error
    }
  }
}
