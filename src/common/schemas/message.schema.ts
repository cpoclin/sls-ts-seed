import { MessageDocument } from '../interfaces/message.interface'
import { Schema, model } from 'mongoose'

const MessageSchema = new Schema(
  {
    body: {
      type: String,
      required: true,
    },
    from: {
      type: String,
      required: true,
      trim: true,
    },
    to: {
      type: String,
      required: true,
      trim: true,
    },
    recruiterId: {
      type: String,
      trim: true,
      index: true,
    },
    conversationId: {
      type: Schema.Types.ObjectId,
      ref: 'conversation',
      index: true,
    },
  },
  {
    versionKey: false,
    timestamps: true,
  }
)

MessageSchema.index({
  from: 1,
  to: 1,
})

MessageSchema.index({
  conversationId: 1,
  recruiterId: 1,
})

export default model<MessageDocument>('message', MessageSchema)
