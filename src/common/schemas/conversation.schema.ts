import { Schema, model } from 'mongoose'
import { ConversationDocument } from '../interfaces/conversation.interface'

const ConversationSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true,
    },
    status: {
      type: String,
      default: 'active',
      enum: ['active', 'deactivated'],
      index: true,
    },
    identities: {
      type: [String],
      default: [],
    },
    participants: {
      type: [String],
      default: [],
    },
  },
  {
    versionKey: false,
    timestamps: true,
  }
)

export default model<ConversationDocument>('conversation', ConversationSchema)
