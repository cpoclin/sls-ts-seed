import {
  EventFactory,
  EventTransporter,
} from '@quantum-talent/lib-ts-event-handler'
import { QTEvent } from '../interfaces/event.interface'

export class EventSender {
  static async putMessage(
    region: string,
    type: EventTransporter,
    event: QTEvent
  ): Promise<void> {
    const producer = new EventFactory(region, type)
    await producer.putMessage(event)
  }
}
