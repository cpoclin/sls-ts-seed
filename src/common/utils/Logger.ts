import { LoggerWrapper } from '@quantum-talent/libs-ts-logger'

export default class Logger {
  private static logger = new LoggerWrapper.Logger(
    'sls-ts-candidate-contact',
    'development' === process.env.NODE_ENV
      ? LoggerWrapper.LoggerLevel.DEBUG
      : LoggerWrapper.LoggerLevel.INFO
  )

  static getLogger(): LoggerWrapper.Logger {
    return this.logger
  }
}
