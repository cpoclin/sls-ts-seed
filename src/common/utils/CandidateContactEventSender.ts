import { EventTransporter } from '@quantum-talent/lib-ts-event-handler'
import { QTEvent } from '../interfaces/event.interface'
import { EventSender } from './EventSender'
import { RecoveryDataSender } from './RecoveryDataSender'

export class CandidateContactEventSender {
  static async putMessage(
    detailType: string,
    eventBody: Record<string, unknown>
  ): Promise<void> {
    const event: QTEvent = {
      eventParams: {
        detailType,
        sqsUrl: '',
        eventBusName: 'recruiters',
        source: 'recruiters.sls-ts-candidate-contact',
      },
      eventBody,
    }
    try {
      await EventSender.putMessage(
        'us-east-1',
        EventTransporter.EventBridge,
        event
      )
    } catch (error) {
      await RecoveryDataSender.putMessage(event)
    }
  }
}
