import axios, { AxiosResponse } from 'axios'
import Logger from './Logger'

export abstract class HttpClient {
  protected async get(
    url: string,
    config?: Record<string, unknown>
  ): Promise<any> {
    try {
      const result: AxiosResponse<any> = await axios.get(url, config)

      return result.data
    } catch (error) {
      Logger.getLogger().error(error)
      throw error
    }
  }

  protected async post(
    url: string,
    data?: Record<string, unknown>,
    config?: Record<string, unknown>
  ): Promise<any> {
    try {
      const result: AxiosResponse<any> = await axios.post(url, data, config)

      return result.data
    } catch (error) {
      Logger.getLogger().error(error)
      throw error
    }
  }

  protected async put(
    url: string,
    data?: Record<string, unknown>,
    config?: Record<string, unknown>
  ): Promise<any> {
    try {
      const result: AxiosResponse<any> = await axios.put(url, data, config)

      return result.data
    } catch (error) {
      Logger.getLogger().error(error)
      throw error
    }
  }
}
