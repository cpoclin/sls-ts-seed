/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
interface Response {
  statusCode: number
  headers: Record<string, unknown>
  body: string
}

export default class ResponseUtils {
  static Success = (body: any): Response => {
    return {
      statusCode: 200,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
      },
      body: JSON.stringify(body),
    }
  }

  static Error = (error: Error): Response => {
    return {
      statusCode: 400,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
      },
      body: JSON.stringify({ message: error.message }),
    }
  }
}
