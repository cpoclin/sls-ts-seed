import { HttpClient } from './HttpClient'

export class CustomApi extends HttpClient {
  async execGet(url: string, config?: Record<string, unknown>): Promise<any> {
    return await this.get(url, config)
  }

  async execPost(
    url: string,
    data?: Record<string, unknown>,
    config?: Record<string, unknown>
  ): Promise<any> {
    return await this.post(url, data, config)
  }

  async execPut(
    url: string,
    data?: Record<string, unknown>,
    config?: Record<string, unknown>
  ): Promise<any> {
    return await this.put(url, data, config)
  }
}
