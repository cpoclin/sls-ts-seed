import { EventTransporter } from '@quantum-talent/lib-ts-event-handler'
import { QTEvent } from '../interfaces/event.interface'
import { EventSender } from './EventSender'

export class RecoveryDataSender {
  static async putMessage(eventBody: QTEvent): Promise<void> {
    const sqsUrl = process.env.SQS_RECOVERY_DATA_DL
    const event: QTEvent = {
      eventParams: {
        detailType: '',
        sqsUrl,
        eventBusName: '',
        source: 'recruiters.sls-ts-candidate-contact',
      },
      eventBody,
    }
    await EventSender.putMessage('us-east-1', EventTransporter.Sqs, event)
  }
}
