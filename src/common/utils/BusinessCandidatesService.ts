import { CustomApi } from './CustomApi'

export class BusinessCandidatesService {
  public static getProcessesCounters = async (
    data: Record<string, unknown>
  ): Promise<Record<string, unknown>> => {
    const customApi: CustomApi = new CustomApi()
    const businessCandidatesUrl: string =
      process.env.BUSINESS_CANDIDATES_URL || ''
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    }
    const processesCounters: Record<string, unknown> = await customApi.execPost(
      `${businessCandidatesUrl}/application-status/application-status-counters-bulk/`,
      data,
      config
    )
    return processesCounters
  }
}
