import mongoose from 'mongoose'
import Logger from './Logger'

const uri = process.env.MONGO_URI

export function isConnected(): boolean {
  return mongoose.connection.readyState === mongoose.STATES.connected
}

export async function connect(): Promise<typeof mongoose> {
  const mongoConnection = await mongoose.connect(uri, {
    useNewUrlParser: true,
    useCreateIndex: true,
    bufferCommands: false,
    bufferMaxEntries: 0,
  })
  return mongoConnection
}

export async function getConnection(
  currentConnection: typeof mongoose
): Promise<typeof mongoose> {
  let connection = currentConnection
  if (!connection || !isConnected()) {
    connection = await connect()
    Logger.getLogger().info(`NEW CONNECTION TO MONGODB ESTABLISHED`)
  } else {
    Logger.getLogger().info(`OLD CONNECTION TO MONGODB RECYCLED`)
  }
  return connection
}
