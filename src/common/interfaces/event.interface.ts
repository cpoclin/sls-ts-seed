interface Params {
  detailType: string
  sqsUrl: string
  eventBusName: string
  source: string
}

export interface QTEvent {
  eventParams: Params
  eventBody: Record<string, unknown> | QTEvent
}
