import { Document } from 'mongoose'

export interface Conversation {
  name: string
  status: string
  identities: Array<string>
  participants: Array<string>
}

export interface ConversationDocument extends Conversation, Document {}
