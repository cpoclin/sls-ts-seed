import { Document } from 'mongoose'
import { ConversationDocument } from './conversation.interface'

export interface Message {
  body: string
  from: string
  to: string
  recruiterId?: string
  conversationId: ConversationDocument
}

export interface MessageDocument extends Message, Document {}
