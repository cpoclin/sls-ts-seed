import { MessageDocument } from '../interfaces/message.interface'
import MessageModel from '../schemas/message.schema'

export class MessagesRepository {
  find = async (): Promise<Array<MessageDocument>> => {
    return await MessageModel.find()
  }

  findOne = async (id: string): Promise<MessageDocument> => {
    return await MessageModel.findOne({ _id: id })
  }
}
