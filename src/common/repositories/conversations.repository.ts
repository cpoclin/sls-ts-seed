import { ConversationDocument } from '../interfaces/conversation.interface'
import ConversationModel from '../schemas/conversation.schema'
import { CreateConversationDto } from '../../conversations/dto/create-conversation.dto'

export class ConversationsRepository {
  create = async (
    body: CreateConversationDto
  ): Promise<ConversationDocument> => {
    const conversation = new ConversationModel(body)
    return await conversation.save()
  }

  find = async (): Promise<Array<ConversationDocument>> => {
    return await ConversationModel.find()
  }

  findOne = async (id: string): Promise<ConversationDocument> => {
    return await ConversationModel.findOne({ _id: id })
  }

  close = async (id: string): Promise<ConversationDocument> => {
    const filter = { _id: id }
    const update = { status: 'deactivated' }
    const options = { new: true }
    return await ConversationModel.findOneAndUpdate(filter, update, options)
  }
}
