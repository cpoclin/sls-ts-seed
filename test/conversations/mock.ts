import { APIGatewayProxyEvent } from 'aws-lambda'

export const conversationBody = {
  name: 'CAMILO',
}

export const conversationPathParameters = {
  id: '60b956e00988e3000984ecf2',
} as Record<string, unknown>

export const conversationEvent = {
  body: JSON.stringify(conversationBody),
  pathParameters: conversationPathParameters,
} as APIGatewayProxyEvent

export const createConversationResponse = {
  status: 'active',
  identities: [],
  participants: [],
  _id: '60b956e00988e3000984ecf2',
  name: 'CAMILO',
  createdAt: '2021-06-03T22:25:36.772Z',
  updatedAt: '2021-06-03T22:25:36.772Z',
}

export const closeConversationResponse = {
  status: 'deactivated',
  identities: [],
  participants: [],
  _id: '60b956e00988e3000984ecf2',
  name: 'CAMILO',
  createdAt: '2021-06-03T22:25:36.772Z',
  updatedAt: '2021-06-03T22:25:36.772Z',
}
