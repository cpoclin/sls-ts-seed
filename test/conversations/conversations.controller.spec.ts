// eslint-disable-next-line @typescript-eslint/no-var-requires
const mockingoose = require('mockingoose')
import { ConversationsController } from '../../src/conversations/conversations.controller'
import ConversationModel from '../../src/common/schemas/conversation.schema'

import * as Mock from './mock'

let conversationsController: ConversationsController

beforeEach(() => {
  conversationsController = new ConversationsController()
})

describe('@ConversationsController', () => {
  it('Should have conversationsController object to be defined', () => {
    expect(
      conversationsController instanceof ConversationsController
    ).toBeTruthy()
  })

  describe('createConversation', () => {
    it('Should create a conversation', async () => {
      mockingoose(ConversationModel).toReturn(
        Mock.createConversationResponse,
        'save'
      )
      const result = await conversationsController.create(
        Mock.conversationEvent
      )
      expect(result.name).toEqual(Mock.conversationBody.name)
    })

    it('Should throw an error', async () => {
      mockingoose(ConversationModel).toReturn(new Error('error'), 'save')
      try {
        await conversationsController.create(Mock.conversationEvent)
      } catch (error) {
        expect(error.message).toEqual('error')
      }
    })
  })

  describe('closeConversation', () => {
    it('Should close a conversation', async () => {
      mockingoose(ConversationModel).toReturn(
        Mock.closeConversationResponse,
        'findOneAndUpdate'
      )
      const result = await conversationsController.close(Mock.conversationEvent)
      expect(result.status).toEqual(Mock.closeConversationResponse.status)
    })

    it('Should throw an error', async () => {
      mockingoose(ConversationModel).toReturn(
        new Error('error'),
        'findOneAndUpdate'
      )
      try {
        await conversationsController.close(Mock.conversationEvent)
      } catch (error) {
        expect(error.message).toEqual('error')
      }
    })
  })
})
